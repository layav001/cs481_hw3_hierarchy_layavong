﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HierarchyApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page3 : ContentPage
	{
		public Page3 ()
		{
			InitializeComponent ();
		}

        async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page4());
        }

        private void New_Appearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Welcome!", "When you leave this page it will change its opacity to .3", "Ok");

        }

        private void New_Disappearing(object sender, EventArgs e)
        {
            ContentPage content = lbj;
            lbj.Opacity = .3;
        }
    }
}