﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HierarchyApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page2 : ContentPage
	{
		public Page2 ()
		{
			InitializeComponent ();
		}

        async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3());
            


        }

        private void Change_Disappearing(object sender, EventArgs e)
        {
            ContentPage content = diff;
            diff.Title = "GREATEST LAKER IN HISTORY";
        }

        private void Diff_Appearing(object sender, EventArgs e)
        {
            ContentPage content = diff;
            diff.Title = "RIP KOBE BEAN BRYANT";
        }
    }
}