﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HierarchyApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page4 : ContentPage
	{
		public Page4 ()
		{
			InitializeComponent ();
		}

        async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PopToRootAsync(); 
        }

        private void ContentPage_Appearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Welcome!", "You are viewing the real GOATs page", "Ok");
        }

        private void ContentPage_Disappearing(object sender, EventArgs e)
        {
            base.DisplayAlert("Goodbye!", "You are leaving the real GOATs page", "Ok");
        }

        private void ImageButton_Clicked(object sender, EventArgs e)
        {

        }
    }
}