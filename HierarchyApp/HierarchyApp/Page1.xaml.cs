﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace HierarchyApp
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Page1 : ContentPage
	{
		public Page1 ()
		{
			InitializeComponent ();
		}

        async void Button_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2());
        }

        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            ContentPage content = bg;
            await Task.Delay(500);
            bg.BackgroundColor = Color.Black;
        }

        private void Bg_Disappearing(object sender, EventArgs e)
        {
            ContentPage content = bg;
            //await Task.Delay(500);
            bg.BackgroundColor = Color.Red;
        }
    }
}